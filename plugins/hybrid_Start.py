#!/usr/bin/python
# Trace slowstart:    Trace TCP congestion_control:slow start
#Author: Dongxu
 

from __future__ import print_function
from bcc import BPF
from bcc.containers import filter_by_containers
from bcc.utils import printb
import argparse
from socket import inet_ntop, ntohs, AF_INET, AF_INET6
from struct import pack
from time import sleep
from datetime import datetime



bpf_text = """
#include <uapi/linux/ptrace.h>
#include <net/sock.h>
#include <bcc/proto.h>
#include <linux/sched.h>
#include <net/tcp.h>
#include <net/inet_connection_sock.h>
#include <linux/tcp.h>
#include <net/tcp_states.h>
#include <linux/skbuff.h>

struct slowstart_info{
    u32 snd_cwnd;
    u32 snd_ssthresh;
    u8 is_found;
    u32	now;
    u32	round_start;
    u32	half_delay;

};

struct bictcp {
	u32	cnt;		/* increase cwnd by 1 after ACKs */
	u32	last_max_cwnd;	/* last maximum snd_cwnd */
	u32	last_cwnd;	/* the last snd_cwnd */
	u32	last_time;	/* time when updated last_cwnd */
	u32	bic_origin_point;/* origin point of bic function */
	u32	bic_K;		/* time to origin point
				   from the beginning of the current epoch */
	u32	delay_min;	/* min delay (msec << 3) */
	u32	epoch_start;	/* beginning of an epoch */
	u32	ack_cnt;	/* number of acks */
	u32	tcp_cwnd;	/* estimated tcp cwnd */
	u16	unused;
	u8	sample_cnt;	/* number of samples to decide curr_rtt */
	u8	found;		/* the exit point is found? */
	u32	round_start;	/* beginning of each round */
	u32	end_seq;	/* end_seq of the round */
	u32	last_ack;	/* last time when the ACK spacing is close */
	u32	curr_rtt;	/* the minimum rtt of current round */
};

BPF_HASH(currsock, struct sock *, struct task_struct *);
BPF_PERF_OUTPUT(SlowStart_events);

int trace_slowstart_entry(struct pt_regs *ctx){
    struct task_struct *t = (struct task_struct *)bpf_get_current_task();
    struct sock *sk = (struct sock *)PT_REGS_PARM1(ctx);
    struct tcp_sock *tp =(struct tcp_sock *)sk;
    int flag= tcp_in_slow_start(tp);
    if(tp->snd_cwnd>10)
          currsock.update(&sk,&t);
    return 0;

}

int trace_slowstart_return(struct pt_regs *ctx){
    struct task_struct **t;
    struct sock *sk = (struct sock *)PT_REGS_PARM1(ctx);
    struct tcp_sock *tp =(struct tcp_sock *)sk;
    t = currsock.lookup(&sk);
    if (t == 0) {
        return 0;   
    }

    struct bictcp *ca = inet_csk_ca(sk);
    if(ca==0){
        return 0;
    }
    struct slowstart_info info = {};
    info.snd_cwnd=tp->snd_cwnd;
    info.snd_ssthresh=tp->snd_ssthresh;
    info.is_found=ca->found;
    info.now=ca->last_ack;
    info.round_start=ca->round_start;
    info.half_delay=ca->delay_min;

    SlowStart_events.perf_submit(ctx, &info, sizeof(info));
    currsock.delete(&sk);
    return 0;
}
"""

def tcpfound2str(found):
    tcpfound={
    0:"NO_FOUND",
    1:"HYSTART_ACK_TRAIN",
    2:"HYSTART_DELAY",
    }
    if found in tcpfound:
        return tcpfound[found]
    else:
        return str(found)

# process event
def print_slowstart_event(cpu, data, size):
    event = b["SlowStart_events"].event(data)
    found=tcpfound2str(event.is_found)
    print("%-6d %-12d %-20.20s %-12.12s %-12.12s %-12s" % (event.snd_cwnd, event.snd_ssthresh,found,event.now,event.round_start,event.half_delay))
# initialize BPF
b = BPF(text=bpf_text)
b.attach_kprobe(event="bictcp_acked", fn_name="trace_slowstart_entry")
b.attach_kretprobe(event="bictcp_acked", fn_name="trace_slowstart_return")


print("Tracing connect ... Hit Ctrl-C to end")

print("%-6s %-12s %-20.20s %-12s %-12s %-12s" % ("CWND", "SSTHRESH", "FOUND", "NOW", "ROUND_START", "DELAY_MIN"))
    
    # read events
b["SlowStart_events"].open_perf_buffer(print_slowstart_event)

while True:
        try:
            b.perf_buffer_poll()
        except KeyboardInterrupt:
            exit()
